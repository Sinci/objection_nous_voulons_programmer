
public class Cercle {
	
	Point center;
	float radius;
	
	//default constructor
	public Cercle(){
		center = new Point();
		radius = 0;
	}
	
	//constructor with three floats
	public Cercle(float pCenterX, float pCenterY,float pRadius){
		center = new Point(pCenterX, pCenterY);
		radius = pRadius;
	}
	
	//constructor with a point and radius
	public Cercle(Point pCenter, float pRadius){
		center = pCenter;
		radius = pRadius;
	}	
	
}
