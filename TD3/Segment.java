
public class Segment {
	Point a;
	Point b;
	
	//default constructor
	public Segment(){
		a = new Point();
		b = new Point();
	}
	
	//constructor with four floats
	public Segment(float pAX, float pAY,float pBX,float pBY){
		a = new Point(pAX, pAY);
		b = new Point(pBX, pBY);
	}
	
	//constructor with x = y
	public Segment(Point pA, Point pB){
		a = pA;
		b = pB;
	}
	
	
}
