
public class Point {
	private float absciss;
	private float ordinate;
	
	//default constructor
	public Point(){
		absciss = 0;
		ordinate = 0;
	}
	
	//constructor with x = y
	public Point(float pCoordinate){
		absciss = pCoordinate;
		ordinate = pCoordinate;
	}
	
	//constructor when x and y are diferents
	public Point(float pAbsciss, float pOrdinate){
		absciss = pAbsciss;
		ordinate = pOrdinate;
	}

}
