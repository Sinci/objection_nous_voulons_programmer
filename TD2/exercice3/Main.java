package exercice3;

import java.util.Scanner;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//exercice 3 //refaire sous forme d'un tableau a deux dimensions
				int studentsNb;
				Scanner sc = new Scanner(System.in);
				//create arrays
				System.out.println("Nombre d'éleves ?");
				studentsNb = sc.nextInt();
				sc.nextLine();
				String studentsNamesTab[] = new String[studentsNb];
				int studentsNotesTab[] = new int[studentsNb];
				for(int i = 0; i < studentsNb; i++){
					System.out.println("Nom de l'éleves " + i);
					studentsNamesTab[i] = sc.nextLine();
					System.out.println("Note de l'éleves " + i);
					studentsNotesTab[i] = sc.nextInt();
					sc.nextLine();
				}
				
				//sort arrays
				sortArrays(studentsNamesTab, studentsNotesTab);
				
				//display
				for(int i =0; i < studentsNb; i++) System.out.println("L'étudiant n°" + (studentsNb-i) + " est " + studentsNamesTab[i] + " avec une moyenne de " + studentsNotesTab[i]);

	}
	
	public static void sortArrays(String names[], int notes[]){
		for(int i = 0; i < names.length; i++){
			for(int j = 1; j < names.length; j++){
				if(notes[j]<notes[j-1]){
					//change for note's tab
					int auxInt = notes[j];
					notes[j] = notes[j-1];
					notes[j-1] = auxInt;
					//change for name's tab
					String auxStr = names[j];
					names[j] = names[j-1];
					names[j-1] = auxStr;
				}
			}
		}
	}
}
