import java.util.Scanner;


public class main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
			}
	
	
	
	public static String[] createTabString(){
		//create tab with user's lenght
		Scanner sc = new Scanner(System.in);
		int lenght;
		System.out.println("Taille du tableau ?");
		lenght = sc.nextInt();
		sc.nextLine();
		String str[] = new String[lenght];
		
		//fill tab
		for(int i = 0; i < lenght; i++){
			System.out.println("Élément " + i + " ?");
			str[i] = sc.nextLine();
		}
		
		//return tab
		return str;
	}
	
	public static double[] createTabDouble(){
		//create tab with user's lenght
		Scanner sc = new Scanner(System.in);
		int lenght;
		System.out.println("Taille du tableau ?");
		lenght = sc.nextInt();
		sc.nextLine();
		double doubleTab[] = new double[lenght];
		
		//fill tab
		for(int i = 0; i < lenght; i++){
			System.out.println("Élément " + i + " ?");
			doubleTab[i] = sc.nextDouble();
			sc.nextLine();
		}
		
		//return tab
		return doubleTab;
	}
	
	public static void displayTab(double tab[]){
		if(tab.length > 0){
			System.out.print("[" + tab[0]);
			for(int i = 1; i < tab.length; i++){
				System.out.print("," + tab[i]);
			}
			System.out.print("]\n");
		}
		else{
			System.out.println("[]");
		}
	}	

	public static void displayTab(String tab[]){
		if(tab.length > 0){
			System.out.print("[" + tab[0]);
			for(int i = 1; i < tab.length; i++){
				System.out.print("," + tab[i]);
			}
			System.out.print("]\n");
		}
		else System.out.println("[]");
	}
	
	public static void copyZone(String tab1[], String tab2[], int start, int end){
		if(tab1.length > end && tab2.length > end){
			for(int i = start; i <= end; i++){
				tab1[i] = tab2[i];
			}
		}
	}
	
	public static void copyZone(double tab1[], double tab2[], int start, int end){
		if(tab1.length > end && tab2.length > end){
			for(int i = start; i <= end; i++){
				tab1[i] = tab2[i];
			}
		}
	}
	
	
	public static String[] enlarge(String tab[], int length){
		String enlargeTab[] = new String[length];
		for(int i = 0; i <tab.length; i++){
			enlargeTab[i] = tab[i];
		}
		return enlargeTab;
	}
	
	public static double[] enlarge(double tab[], int length){
		double enlargeTab[] = new double[length];
		for(int i = 0; i < tab.length; i++){
			enlargeTab[i] = tab[i];
		}
		return enlargeTab;
	}
	
	public static String[] fusion(String tab1[], String tab2[]){
		String newTab[] = new String[tab1.length + tab2.length];
		for(int i = 0; i < tab1.length; i++){
			newTab[i] = tab1[i];
		}
		for(int i = tab1.length; i< newTab.length; i++){
			newTab[i] = tab2[i-tab1.length];
		}
		return newTab;
	}
	
	public static double[] fusion(double tab1[], double tab2[]){
		double newTab[] = new double[tab1.length + tab2.length];
		for(int i = 0; i < tab1.length; i++){
			newTab[i] = tab1[i];
		}
		for(int i = tab1.length; i< newTab.length; i++){
			newTab[i] = tab2[i-tab1.length];
		}
		return newTab;
	}
	
	public static String[] insert(String tab[], String str, int indice){
		String newTab[] = new String[tab.length + 1];
		if(indice <= tab.length){
			for(int i = 0; i < indice; i++) newTab[i] = tab[i];
			newTab[indice] = str;
			for(int i = indice + 1; i < newTab.length; i++) newTab[i] = tab[i-1];
		}
		return newTab;
	}
	
	public static double[] insert(double tab[], double str, int indice){
		double newTab[] = new double[tab.length + 1];
		if(indice <= tab.length){
			for(int i = 0; i < indice; i++) newTab[i] = tab[i];
			newTab[indice] = str;
			for(int i = indice + 1; i < newTab.length; i++) newTab[i] = tab[i-1];
		}
		return newTab;
	}
	
	public static String[] delete(String tab[], int indice){
		String newTab[] = new String[tab.length - 1];
		if(indice <= tab.length){
			for(int i = 0; i < indice; i++) newTab[i] = tab[i];
			for(int i = indice; i < newTab.length; i++) newTab[i] = tab[i+1];
		}
		return newTab;
	}
	
	public static double[] delete(double tab[], int indice){
		double newTab[] = new double[tab.length - 1];
		if(indice <= tab.length){
			for(int i = 0; i < indice; i++) newTab[i] = tab[i];
			for(int i = indice; i < newTab.length; i++) newTab[i] = tab[i+1];
		}
		return newTab;
	}
	
	public static String[] delete(String tab[], String str){
		String newTab[] = new String[tab.length - 1];
		boolean find = false;
		for(int i = 0; i < tab.length; i++) {
			if(find) newTab[i] = tab[i+1];
			else if(tab[i] == str) find = true;
			else if(i < tab.length - 1) newTab[i] = tab[i];
		}
		if(find) return newTab;
		else return tab;
	}
	
	public static double[] delete(double tab[], double nb){
		double newTab[] = new double[tab.length - 1];
		boolean find = false;
		for(int i = 0; i < tab.length; i++) {
			if(find) newTab[i] = tab[i+1];
			else if(tab[i] == nb) find = true;
			else if(i < tab.length - 1) newTab[i] = tab[i];
		}
		if(find) return newTab;
		else return tab;
	}
	
	
}
