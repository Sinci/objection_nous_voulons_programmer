package exercice2;

import java.util.Scanner;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double doubleTab[] = createTabDouble();
		byte sortChoose;
		do{
			System.out.println("choississez votre algorithme de tri :");
			System.out.println("1 > bulle");
			System.out.println("2 > fusion");
			sortChoose = sc.nextByte();
		}while (sortChoose != 1 && sortChoose !=2);
		if(sortChoose == 1) bulleSort(doubleTab);
		else fusionSort(doubleTab, 0 , doubleTab.length-1);
		displayTab(doubleTab);

	}
	
	public static void bulleSort(double tab[]){
		for(int i = 0; i < tab.length; i++){
			for(int j = 1; j < tab.length; j++){
				if(tab[j]<tab[j-1]){
					double aux = tab[j];
					tab[j] = tab[j-1];
					tab[j-1] = aux;
				}
			}
		}
	}
	
	public static void fusionSort(double tab[], int start, int end){
		displayTab(tab);
		if(start != end){
			int middle = (start+end)/2;
			fusionSort(tab, start, middle);
			fusionSort(tab, middle +1, end);
			fusion(tab, start, middle ,end);
		}
	}
	
	
	public static void fusionSort(double tab[]){
		fusionSort(tab, 0, tab.length-1);
	}
	
	
	private static void fusion(double array[],int start1,int end1,int end2)
    {
	int start2 = end1 + 1;

    //on recopie les éléments du début du tableau
	double arrAux[] = new double[end1 - start1 + 1];
	for(int i = start1; i <=end1; i++) arrAux[i- start1] = arrAux[i];
    
	int iTab1 = start1;
	int iTab2 = start2;
    
	for(int i = start1; i <= end2; i++){
		if(iTab1 == start2){
			break;
		}
		else if(iTab2 == (end2+1)){
			array[i] = arrAux[ iTab1 - start1];
			iTab1++;
		}
		else if(arrAux[iTab1 - start1]<array[iTab2]){
			array[i] = arrAux[iTab1 - start1];
			iTab1++;
		}
        else
            {
        	array[i]=array[iTab2]; 
            iTab2++;
            }
        }
    }
	

	
	public static double[] createTabDouble(){
		//create tab with user's length
		Scanner sc = new Scanner(System.in);
		int lenght;
		System.out.println("Taille du tableau ?");
		lenght = sc.nextInt();
		sc.nextLine();
		double doubleTab[] = new double[lenght];
		
		//fill tab
		for(int i = 0; i < lenght; i++){
			System.out.println("Élément " + i + " ?");
			doubleTab[i] = sc.nextDouble();
			sc.nextLine();
		}
		
		//return tab
		return doubleTab;
	}
	
	public static void displayTab(double tab[]){
		if(tab.length > 0){
			System.out.print("[" + tab[0]);
			for(int i = 1; i < tab.length; i++){
				System.out.print("," + tab[i]);
			}
			System.out.print("]\n");
		}
		else{
			System.out.println("[]");
		}
	}	
	
	

}
