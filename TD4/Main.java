
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Point a = new Point("a", 3, 4);
		Point b = new Point("b", 4, 2);
		Point c = new Point("c", -4, 2);
		Point d = new Point("d", 4, -4);
		Point e = new Point("e", -1, 1);
		Point f = new Point("f", 1, -4);
		Point g = new Point("g", 2, -1);
		Point h = new Point("h", 3, -2);
		Point i = new Point("i", -3, -1);
		Point j = new Point("j", -2, -2);
		Point border1 = new Point("", -4, 4);
		Point border2 = new Point("", 4, -4);
		Rectangle ab = new Rectangle(a,b);
		Rectangle cd = new Rectangle(c,d);
		Rectangle ef = new Rectangle(e,f);
		Rectangle gh = new Rectangle(g,h);
		Rectangle ij = new Rectangle(i,j);
		Rectangle border = new Rectangle(border1,border2);
		Drawing drawing = new Drawing();
		drawing.add(ab);
		drawing.add(cd);
		drawing.add(ef);
		drawing.add(gh);
		drawing.add(ij);
		drawing.display();
		System.out.println(border.info()+ "\n" + border.area());
		Point.setOrigin(d);
		ab.symmetry();
		gh.symmetry();
		border1.translate(0,-2);
		border2.translate(2,-8);
		drawing.display();
		System.out.println(border.info()+ "\n" + border.area());
		
		
		
	}

}
