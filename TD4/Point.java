
public class Point {
	private float absciss;
	private float ordinate;
	private String name;
	private static float originAbsciss = 0;
	private static float originOrdinate = 0;
	
	//default constructor
	public Point(String pName, float pAbsciss, float pOrdinate){
		name = pName;
		absciss = pAbsciss;
		ordinate = pOrdinate;
	}
	
	//accessor 
	public float getAbs(){
		return absciss;
	}
	
	public float getOrd(){
		return ordinate;
	}
	
	public String getName(){
		return name;
	}
	
	//mutator
	public void setAbs(float pAbsciss){
		absciss = pAbsciss;
	}
	
	public void setOrd(float pOrdinate){
		ordinate = pOrdinate;
	}
	
	static public void setOrigin(Point point){
		originAbsciss = point.absciss;
		originOrdinate = point.ordinate;
	}
	
	
	//methods
	
	public float getX(){
		return absciss - originAbsciss;
	}
	
	public float getY(){
		return ordinate - originOrdinate;
	}
	
	public boolean pegX(Point point){
		return absciss <= point.absciss;
	}
	
	public boolean pegY(Point point){
		return ordinate <= point.ordinate;
	}
	
	public void translate(float addToAbsciss, float addToOrdinate){
		absciss += addToAbsciss;
		ordinate += addToOrdinate;
	}
	
	public void symmetry(){
		absciss = originAbsciss - this.getX();
		ordinate = originOrdinate - this.getY();
	}
	
	public String info(){
		return name + "(" + absciss + "," + ordinate + ")";
	}

}