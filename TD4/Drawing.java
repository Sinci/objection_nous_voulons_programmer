
public class Drawing {
	private Rectangle rectangles[];
	
	public Drawing(){
		rectangles = new Rectangle[0];
	}
	
	public void add(Rectangle rectangle){
		Rectangle newRectangles[] = new Rectangle[rectangles.length +1];
		for(int i = 0; i<rectangles.length; i++) newRectangles[i] = rectangles[i];
		newRectangles[rectangles.length ] = rectangle;
		rectangles = newRectangles;
	}
	
	public void translate(float addToAbsciss, float addToOrdinate){
		for(int i = 0; i<rectangles.length; i++) rectangles[i].translate(addToAbsciss, addToOrdinate);
	}
	
	public void symmetry(){
		for(int i = 0; i<rectangles.length; i++) rectangles[i].symmetry();
	}
	
	public float area(){
		float area = 0;
		for(int i = 0; i<rectangles.length; i++) area += rectangles[i].area();
		return area;
	}
	
	public void display(){
		for(int i = 0; i<rectangles.length; i++) System.out.println(rectangles[i].info());
	}
}
