
public class Rectangle {
	private Point a;
	private Point b;
	private String name;
	
	//constructor
	public Rectangle(Point pA, Point pB){
		a = pA;
		b = pB;
		name = pA.getName() + pB.getName();
	}
	
	//accessor 
	public float getSmallerAbs(){
		if(a.getAbs()< b.getAbs()) return a.getAbs();
		else return b.getAbs();
	}
	
	public float getBiggerAbs(){
		if(a.getAbs()< b.getAbs()) return b.getAbs();
		else return a.getAbs();
	}
	
	public float getSmallerOrd(){
		if(a.getOrd()< b.getOrd()) return a.getOrd();
		else return b.getOrd();
	}
	
	public float getBiggerOrd(){
		if(a.getOrd()< b.getOrd()) return b.getOrd();
		else return a.getOrd();
	}
	
	//methods
	public void translate(float addToAbsciss, float addToOrdinate){
		a.translate(addToAbsciss, addToOrdinate);
		b.translate(addToAbsciss, addToOrdinate);
	}
	
	public void symmetry(){
		a.symmetry();
		b.symmetry();
	}
	
	public float area(){
		return (this.getBiggerAbs() - this.getSmallerAbs())* (this.getBiggerOrd() - this.getSmallerOrd());
	}
	
	public String info(){
		return name + "[" + a.info() + "," + b.info() + "]";
	}
	

}
