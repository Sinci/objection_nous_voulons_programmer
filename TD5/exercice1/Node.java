
public abstract class Node {
	protected String name;
	public InternalNode childs[] = new InternalNode[0];
	protected Node parent;
	
	public Node(String pName){
		name = pName;
	}
	
	protected void addChildToChilds(InternalNode child){
		InternalNode newChildArray[] = new InternalNode[childs.length + 1];
		for(int i = 0; i < childs.length; i++) newChildArray[i] = childs[i];
		newChildArray[childs.length] = child;
		childs = newChildArray;
	}
	
	
	public void addChild(){
		InternalNode newChild = new InternalNode(name + (childs.length + 1), this);
		this.addChildToChilds(newChild);
	}
	
	public InternalNode consultChild(int i){
		if(childs.length<i) return null;
		else return childs[i-1];
	}
	
	public abstract Node consultParent();
	
	public String toString(){
		return name;
	}
	
	
}
