
public class InternalNode extends Node{
	public InternalNode(String pName, Node pParent){
		super(pName);
		parent = pParent;
	}

	public Node consultParent() {
		return parent;
	}
}
