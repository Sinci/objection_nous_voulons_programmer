package exercice2;

abstract public class Vehicle {
	protected int id;
	protected static int incrementationId = 0;
	protected float distanceTraveled;

	protected Vehicle(){
		id = incrementationId++;
		distanceTraveled = 0;
	}
	
	protected Vehicle(float pDistanceTraveled){
		id = incrementationId++;
		distanceTraveled = pDistanceTraveled;
	}
	
	public String toString(){
		return "Je suis un véhicule d'identifiant " + id + " j'ai parcourrue " + distanceTraveled;
	}
	
	public void drive(){
		distanceTraveled *= 2;
	}
}
