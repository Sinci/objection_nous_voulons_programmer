package exercice2;

abstract public class VehicleWithoutEngine extends Vehicle{
	protected VehicleWithoutEngine(){
		super();
	}
	
	protected VehicleWithoutEngine(float pDistanceTraveled){
		super(pDistanceTraveled);
	}
	
	public String toString(){
		return super.toString() +" je suis à moteur";
	}
}
