package exercice2;

public class Truck extends VehicleWithEngine {
	private float transportedVolume;

	public Truck(float pDistanceTraveled, float pTankCapacity, float pLevelOil, float pTransportedVolume){
		super(pDistanceTraveled,pTankCapacity,pLevelOil);
		transportedVolume = pTransportedVolume;
	}
	
	public String toString(){
		return super.toString() +", plus précisément un camion avec une capacité de transport de " + transportedVolume;
	}
}
