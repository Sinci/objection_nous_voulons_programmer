package exercice2;

public class Bycicle extends VehicleWithoutEngine{
	private int speedNb;
	
	public Bycicle(float pDistanceTraveled, int pSpeedNb){
		super(pDistanceTraveled);
		speedNb = pSpeedNb;
	}
	
	public String toString(){
		return super.toString() +", plus précisément un vélo avec " + speedNb + " vitesses";
	}
}
