package exercice2;

public abstract class VehicleWithEngine extends Vehicle{
	protected float tankCapacity;
	protected float levelOil;
	protected boolean breakdown;
	
	protected VehicleWithEngine(){
		super();
		tankCapacity = 0;
		levelOil = 0;
	}
	
	protected VehicleWithEngine(float pDistanceTraveled){
		super(pDistanceTraveled);
	}
	
	protected VehicleWithEngine(float pDistanceTraveled, float pTankCapacity, float pLevelOil){
		super(pDistanceTraveled);
		tankCapacity = pTankCapacity;
		levelOil = pLevelOil;
	}
	
	protected VehicleWithEngine(float pTankCapacity, float pLevelOil){
		super();
		tankCapacity = pTankCapacity;
		levelOil = pLevelOil;
	}
	
	public String toString(){
		String str = super.toString() +" je suis à moteur avec une capacité de " + tankCapacity + " et un niveau d'essance de " + levelOil + " je suis ";
		if (breakdown) str += "en panne";
		else str += "pas en panne";
		return str;
	}
	
	public boolean isBreakdown(){
		if (levelOil == 0){
			breakdown = true;
			return breakdown;
		}
		else {
			return breakdown;
		}
	}
	
	public void provision(float littersNb){
		if(littersNb+levelOil > tankCapacity){
			levelOil=tankCapacity;
			System.out.println("ça déborde !");
		}
		else{
			levelOil += littersNb;
		}
		breakdown = false;
	}
}
