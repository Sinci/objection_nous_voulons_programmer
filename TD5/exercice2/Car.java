package exercice2;

public class Car extends VehicleWithEngine {
	private int nbPlaces;
	
	public Car(float pDistanceTraveled,float pTankCapacity, float pLevelOil, int pNbPlaces){
		super(pDistanceTraveled,pTankCapacity,pLevelOil);
		nbPlaces = pNbPlaces;
	}
	
	public String toString(){
		return super.toString() +", plus précisément une voiture à " + nbPlaces + " place(s)";
	}
}
