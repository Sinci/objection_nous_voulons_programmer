package exercice2;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Vehicle vehicles[] = new Vehicle[4];
		vehicles[0] = new Car(15, 14, 13, 12);
		vehicles[1] = new Truck(10, 20, 30, 40);
		vehicles[2] = new Bycicle(50, 60);
		vehicles[3] = new Car(150, 140, 130, 0);;
		for(Vehicle v : vehicles){
			System.out.println(v);
		}
		vehicles[3].drive();
		System.out.println("Est ce que la 2eme voiture est en panne ? " + ((VehicleWithEngine)vehicles[3]).isBreakdown());
		((VehicleWithEngine)vehicles[3]).provision(15);
		System.out.println(vehicles[3]);
	}

}
